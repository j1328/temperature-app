var express = require('express'),
    router = express.Router();
var db = require("../../database.js")

router
  .get("/:name", (req, res, next) => {
    var sql = "select * from source where name = ?"
    var params = [req.params.name]
    db.get(sql, params, (err, row) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
	if(req.accepts("text")) //allow plain text so arduino doesn't need to parse json
	{
      res.type("txt")
	  res.send(row["id"] + "")
      return;
	}
	if(req.accepts("json"))
	//else //default to json
	{
          res.json({
              "message":"success",
              "data":row
          })
    }
    else
    {
        res.status(400).send("bad response type")
        return;
    }
    });
});

router.post("/", (req, res, next) => {
    var errors=[]
    if (!req.body.name){
        errors.push("No name specified");
    }
    if (errors.length){
        res.status(400).json({"error":errors.join(",")});
        return;
    }
    var data = {
        name: req.body.name,
    }
    var sql ='INSERT INTO source (name) VALUES (?)'
    var params =[data.name]
    var newid =  db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            "message": "success",
            "data": data,
            "id" : this.lastID
        })
    });
})

router.patch("/:id", (req, res, next) => {
    var data = {
        name: req.body.name,
    }
    db.run(
        `UPDATE user set 
           name = COALESCE(?,name), 
           WHERE id = ?`,
        [data.name, req.params.id],
        function (err, result) {
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({
                message: "success",
                data: data,
                changes: this.changes
            })
    });
})

router.delete("/api/source/:id", (req, res, next) => {
    db.run(
        'DELETE FROM source WHERE id = ?',
        req.params.id,
        function (err, result) {
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({"message":"deleted", changes: this.changes})
    });
})

module.exports = router;