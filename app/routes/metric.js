var express = require('express'),
    router = express.Router();
var db = require("../../database.js")

router
  .get("/:name", (req, res, next) => {
    var sql = "select metric.id, source.name AS Source, Temperature, Humidity, Timestamp from metric LEFT JOIN source on source.id = metric.source where name = ? LIMIT ?,?"
	var skip = 0
	var take = 50
	if(req.query.skip)
	{
		skip = req.query.skip
	}
	if(req.query.take)
	{
		take = req.query.take
	}
    var params = [req.params.name, skip, take]
    db.get(sql, params, (err, row) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":row
        })
      });
});

router.post("/", (req, res, next) => {
    var errors=[]
    if (!req.body.source){
        errors.push("No source specified");
    }
    if (errors.length){
        res.status(400).json({"error":errors.join(",")});
        return;
    }
    var data = {
        source: req.body.source,
		temperature: req.body.temperature,
		humidity: req.body.humidity
    }
    var sql ='INSERT INTO metric (Source, Temperature, Humidity) VALUES (?, ?, ?)'
    var params =[data.source, data.temperature, data.humidity]
    var newid =  db.run(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            "message": "success",
            "data": data,
            "id" : this.lastID
        })
    });
})

module.exports = router;