var sqlite3 = require('sqlite3').verbose()
var md5 = require('md5')

const DBSOURCE = "db.sqlite"
const DBCLEAN = "db-clean.sqlite"

const fs = require('fs')

try {
  if (fs.existsSync(DBSOURCE)) {
    //file exists
  }
  else{
    console.log("DB not found, copying clean")
	  fs.copyFile(DBCLEAN, DBSOURCE, (err) => {
	    if (err) {
		    console.log("Error Found:", err);
	    }
	  });
  }
} 
catch(err) {
  console.error(err)
}

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
      // Cannot open database
      console.error(err.message)
      throw err
    }
});


module.exports = db
