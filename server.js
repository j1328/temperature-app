// Create express app
var express = require("express")
var app = express()

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Server port
var HTTP_PORT = 3000
// Start server
app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});
// Root endpoint
app.get("/", (req, res, next) => {
    res.json({"message":"Ok"})
});

var sourceRoutes = require('./app/routes/source');
app.use('/api/source', sourceRoutes);

var metricRoutes = require('./app/routes/metric');
app.use('/api/metric', metricRoutes);

// Default response for any other request
app.use(function(req, res){
    res.status(404);
});
